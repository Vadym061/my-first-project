'use strict';


const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];
const root = document.getElementById('root');

function render(el){
  const list = document.createElement('ul');
  if(el.author && el.name && el.price){
    const listItem = document.createElement('li');
    listItem.innerHTML = `author: ${el.author}<br> name: ${el.name}<br> price: ${el.price}`;
    list.append(listItem);
    root.append(list)
  }
}

function booksList(){
  books.forEach((el, index)=>{
    try{
      if(!el.author){
        throw new Error (`Author data, in the array object under index ${index} there must be a property author`);
      }else if(!el.name){
        throw new Error (`Name data, in the array object under index ${index} there must be a property name`);
      }else if(!el.price){
        throw new Error (`Price data, in the array object under index ${index} there must be a property price`);
      }
      render(el)
    }catch (el){
      console.log(el.name + ': ' + el.message);
    }
  })
}

booksList(books);