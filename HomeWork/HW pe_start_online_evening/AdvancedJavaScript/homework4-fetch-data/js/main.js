'use strict';

const url = 'https://ajax.test-danit.com/api/swapi/films';
const root = document.querySelector('#root');

class MovieInfo {
    constructor(url, root) {
      this.url = url;
      this.root = root;
    }
  
    getInfo() {
      fetch(url)
        .then((response) => {
          return response.json();
        })
        .then((response) => {
          return response.forEach((arr) => {
            const section = document.createElement("h2");
            section.classList.add('title');
            section.append(`Episode: ${arr.episodeId} Title: ${arr.name}`);


            const container = document.createElement('div')
            container.classList.add('container')
            const des = document.createElement("p");
            des.classList.add('description')
            const list = document.createElement("ul");
            list.classList.add('list')
            des.append(arr.openingCrawl);
            container.append(section);
            
            arr.characters.forEach((e) => {
              fetch(e)
                .then((response) => {
                  return response.json();
                })
                .then((response) => {
                  let { name } = response;
                  const item = document.createElement("li");
                  item.classList.add('item')
                  item.textContent = name;
                  list.append(item);
                });
            });

            container.appendChild(des);
            container.appendChild(list);
            root.append(container);
          });
        });
    }
  }
  
  const info = new MovieInfo(url, root);
  
  info.getInfo();
  