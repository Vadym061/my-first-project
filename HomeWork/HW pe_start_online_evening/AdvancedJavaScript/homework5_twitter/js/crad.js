'use strict';


const appCard = function(){
 setTimeout(()=>{
    const USERS_URL = "https://ajax.test-danit.com/api/json/users";
    const POSTS_URL = "https://ajax.test-danit.com/api/json/posts";
    const root = document.querySelector("#root");
    
    const getData = async (url) => {
        const response = await fetch(url);
        const data = await response.json();
        return data;
    };
    
    class Card {
        constructor({ id: postId, userId, title, body }, users) {
            this.postId = postId;
            this.userId = userId;
            this.title = title;
            this.body = body;
            const { name: name, username, email, id } = users[this.userId];
            this.name = name;
            this.username = username;
            this.email = email;
            this.authorId = id; 
        }
    
    
        postCard(parent) {
            const container = document.createElement("div");
            container.classList.add("card");
            container.innerHTML = `
                <div class="btn-delete">
                    <button class="delete">Delete</button>
                </div>
                <h3 class="card-name">${this.name}</h3>
                <h3 class="card-user">${this.username}</h3>
                <a class="card-email" href="mailto:${this.email}">${this.email}</a>
                <h2 class="card-title">${this.title.toUpperCase()}</h2>
                <p class="card-text">${this.body}</p>
            `;
            parent.append(container);
            container
                .querySelector(".delete")
                .addEventListener("click", async () => {
                    const respons = await this.sendRequest(
                        `${POSTS_URL}/${this.postId}`
                    );
                    if (respons.ok) {
                        container.remove();
                    }
                });
        }
    
        async sendRequest(url) {
            const response = await fetch(url, { method: "DELETE" });
            return response;
        }
    }
    
    class Obj {
        constructor(users) {
            this.addUsers(users);
        }
    
        addUsers(users) {
            for (const user of users) {
                this[user.id] = user;
            }
        }
    }
    
    const renderCards = async () => {
        const [usersArray, posts] = await Promise.all([
            getData(USERS_URL),
            getData(POSTS_URL),
        ]);
        const users = new Obj (usersArray);
    
        for (const post of posts) {
            const card = new Card(post, users);
            card.postCard(root);
        }
    };
    
    renderCards();

 }, 3000)

}
export {appCard}