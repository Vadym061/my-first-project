'use strict';

import {modalIcon} from './modal.js';
import {appCard} from './crad.js';

modalIcon();
appCard();

const API = 'https://ajax.test-danit.com/api/json/';
const loader = document.querySelector('.loader');




const sendRequest = async (entity, method = "GET", config) => {
    return await fetch(`${API}${entity}`, {
        method,
        ...config
    }).then(response => {
        if(response.ok){
            return response.json()
        }
    })
}
setTimeout(()=>{
    loader.style.display = 'none'
},3000)


const getUser = (id) => sendRequest(`users/${id}`);
const newPost = (requestBody) => sendRequest('posts', 'POST', {body: JSON.stringify(requestBody)});

const form = document.querySelector('[type="submit"]') 

form.onclick = function(e){
e.preventDefault();
const postTitle = document.querySelector('#postTitle').value
const postText = document.querySelector('#postText').value

const requestBody = {
    userId: 1,
    id: 101,
    body: postText,
    title: postTitle
};

setTimeout(()=>{
    newPost(requestBody)
        .then(({userId, id, body, title}) => {
        getUser(userId).then(
            ({name, email}) => {
                root.insertAdjacentHTML("afterbegin", `
                    <div class="card" id = "post-${id}">
                    <div class="btn-delete">
                        <button class="delete">Delete</button>
                    </div>
                        <h3 class="card-name">${name}</h3>
                        <a class="card-email" href="mailto:${email}">${email}</a>
                        <h2 class="card-title">${title.toUpperCase()}</h2>
                        <p class="card-text">${body}</p>
                    </div>
                `)
        
                document.querySelector('#postTitle').value = ""
                document.querySelector('#postText').value = ""
            
            })            
        })
    }, 3000);
}