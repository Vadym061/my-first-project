'use strict';
const modalIcon = function(){
const btn = document.querySelector('.btn-add');
const modal = document.querySelector('.container-form')
const close = document.querySelector('.btn-close');


function clickModal() {
        btn.addEventListener("click", () => {
            modal.style.display = 'flex';
        })
        close.addEventListener("click", () => {
            modal.style.display = 'none';
        })
}
    

window.addEventListener("click", e => {
    const target = e.target
    if (!target.closest(".btn-add") && !target.closest(".container-form")) {
        modal.style.display = 'none';
    }
});

clickModal()

window.addEventListener("DOMContentLoaded",() => {
	const btnSub = document.querySelector(".btn-submit");
	let doneTimeout = null,
		resetTimeout = null;

if (btnSub) {
	btnSub.addEventListener("click",function() {
		const runClass = "btn--running";
		const doneClass = "btn--done";
		const submitDuration = 2000;
		const resetDuration = 1500;

		this.disabled = true;
		this.classList.add(runClass);

		clearTimeout(doneTimeout);
		clearTimeout(resetTimeout);

		doneTimeout = setTimeout(() => {
			this.classList.remove(runClass);
			this.classList.add(doneClass);
				
			resetTimeout = setTimeout(() => {
				this.disabled = false;
				this.classList.remove(doneClass);
			}, resetDuration);

			}, 600 + submitDuration);
				setTimeout(()=>{
					modal.style.display = 'none'
				},3500)
			});
		}
	});
}

export {modalIcon}
