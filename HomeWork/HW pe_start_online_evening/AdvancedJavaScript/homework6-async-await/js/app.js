const getUser = 'https://api.ipify.org/?format=json';
const btn = document.querySelector('.btn');
const loader = document.querySelector('.preloader');




btn.addEventListener('click', getIp);

async function getIp(){
    loader.style.display = 'flex';
    const user = await fetch(getUser)
    .then((response)=> response.json());

    const location = await fetch(`http://ip-api.com/json/${user.ip}`)
    .then((response)=> response.json())
    const ip = new findIp(location);
    ip.render();
    btn.removeEventListener('click', getIp)
}



class findIp {
    constructor({timezone, status, country, regionName, city, region, zip}){
        this.status = status;
        this.region = region;
        this.timezone = timezone;
        this.country = country;
        this.regionName = regionName;
        this.city = city;
        this.zip = zip;
    }

    render(){
        const root = document.querySelector('.root');
        setTimeout(()=>{
            root.insertAdjacentHTML('beforeend', `
                    <div class="wrapper">
                    <p class="text">Інформацію про ваше місцезнаходження <span class="status">${this.status}</span> отримано!!!</p>
                    <ul class="list-item">
                        <li class="item item1">СБУ вже виїхало за адресою :</li>
                        <li class="item">Країна: ${this.country}, ${this.timezone}</li>
                        <li class="item">Місто: ${this.city},${this.regionName}</li>
                        <li class="item">Регіон: ${this.region}</li>
                        <li class="item">Індекс: ${this.zip}</li>
                    </ul>
                    </div>       
                    `)
                    loader.style.display = 'none'
        },3000)
        
    }
}