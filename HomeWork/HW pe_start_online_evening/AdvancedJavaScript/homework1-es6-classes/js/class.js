'use strict';

// ## Завдання
// 1. Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// 2. Створіть гетери та сеттери для цих властивостей.
// 3. Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// 4. Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// 5. Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

class Employee {
    constructor(name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    set name(value){
        this._name = value;
    }
    get name(){
        return this._name;
    }
    set age(value){
        this._age = value;
    }
    get age(){
        return this._age;
    }
    set salary(value){
        this._salary = value;
    }
    get salary(){
        return this._salary;
    }
}

class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary);
        this._lang = lang;
    }
    get salary(){
        return super.salary * 3;
    }
}

const userOne = new Programmer('Vadym', 30, 40000,['HTML', 'CSS', 'JavaScript', 'React']);
const userTwo = new Programmer('Elena', 35, 30000,['HTML', 'CSS', 'JS', 'Vue']);
const userThree = new Programmer('Sophia',25, 20000,['Java', 'Kotlin', 'PHP']);
console.log(userOne);
console.log(userTwo);
console.log(userThree);