'use strict';

// Технічні вимоги:

// Візьміть виконане домашнє завдання номер 4 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:

// При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
// Створити метод getAge() який повертатиме скільки користувачеві років.
// Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) 
// та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.


// Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.

function createNewUser() {
    let firstName = prompt('Enter your name');
    let lastName = prompt('Enter your last name');
    let birthday = prompt('Enter your date of birth','01/01/2004');

    return{
        firstName,
        lastName,
        birthday,
        getLogin: function (){
            let firstPart = this.firstName[0].toLowerCase();
            let lastPart = this.lastName.toLowerCase();
            return `${firstPart}${lastPart}`
        },
        getAge (){

            let date = new Date();
            let currentYear = date.getFullYear()
            return currentYear - this.birthday.slice(-4)
        },
        getPassword(){
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + (this.birthday.slice(-4))
        }   

    }

    // this.getLogin = function(){
    //     let newLogin = this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
    //        return newLogin;
    // }
    
    // this.getAge = function(){
    //     const dateDays = new Date();
    //     const userBirthday = Date.parse(`${this.birthday.slice(6)} - ${this.birthday.slice(3, 5)} - ${this.birthday.slice(0, 2)}`);
    //     console.log(userBirthday)
    //     const ageUser = ((dateDays - userBirthday) / (1000 * 60 * 60 * 24 * 30 * 12)).toFixed(0);

    //     if(ageUser < dateDays){
    //         return `${ageUser - 1} years`
    //     }else{
    //         return `${ageUser} years`
    //     }
    // }

    // this.getPassword = function(){
    //     return `${this.firstName[0].toUpperCase()}${this.lastName.toLocaleLowerCase()}${this.birthday.slice(-4)}`
    // };
}

let newUser = new createNewUser();
console.log(`Your login: ${newUser.getLogin()}`);
console.log(`Your age: ${newUser.getAge()}`);
console.log(`Your passwoed: ${newUser.getPassword()}`);