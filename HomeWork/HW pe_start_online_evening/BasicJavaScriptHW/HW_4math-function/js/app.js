'use strict';

// Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
// Технічні вимоги:

//     Отримати за допомогою модального вікна браузера два числа.
//     Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /.
//     Створити функцію, в яку передати два значення та операцію.
//     Вивести у консоль результат виконання функції.
let numOne = +prompt('Your first number',2);
let numTwo = +prompt('Your second number',2);
let operator = prompt('Your operator', '+');



function calculate (a, b, operator){
    if(operator === '+'){
        return a + b;
    }else if(operator === '-'){
        return a - b;
    }else if(operator === '*'){
        return a * b;
    }else if(operator === '/'){
        return a / b;
    }else{
        alert('Not correct')
    }
    
}


if(isNaN(numOne) || isNaN(numTwo) || !operator){
        alert('Not correct')
};

console.log(calculate(numOne,numTwo,operator))