'use strict';
// 1. Опишите своими словами разницу между функциями `setTimeout()` и `setInterval()`.
// setTimeout() - вызывает функцию один раз.
// setInterval() - вызывает функцию регулярно раз.

// 2. Что произойдет, если в функцию `setTimeout()` передать нулевую задержку? Сработает ли она мгновенно, и почему?
// Сработает сразу, так как в задержку передается 0 секунд.

// 3. Почему важно не забывать вызывать функцию `clearInterval()`, когда ранее созданный цикл запуска вам уже не нужен?
// Это может вызвать бесконечный цикл или выполнений действий, котрые уже больше не нужны.



let counter = 1;

function sliderInterval() {
  let showSlider = setInterval(() => {
  if  (counter > 4) {
    counter = 1;
  }
  document.querySelector(".image-to-show").src = `./img/${counter}.jpg`;
  counter++ 
  }, 3000);
  
  const btnPause = document.querySelector("#btn-stop");
  btnPause.addEventListener("click", stopShow);
  function stopShow(){
  clearInterval(showSlider);
  }
}

sliderInterval()

const btnPlay = document.querySelector("#btn-play");
btnPlay.addEventListener("click", playShow)
function playShow (){
    sliderInterval()
}

function playShow (){
  sliderInterval()
}