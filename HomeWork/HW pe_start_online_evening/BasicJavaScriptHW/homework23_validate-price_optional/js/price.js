'use strict';


function validatePrice (){
    const message = document.createElement('span');
    const error = document.createElement('error');
    const price = document.getElementById('price');
    const inputs = document.createElement('input');
    const btn = document.createElement('btn');
    btn.innerHTML = `&#10062;`;
    inputs.placeholder = 'PRICE';
    error.textContent = 'Please enter correct price';
    
    inputs.addEventListener('focusin', () => {
        inputs.style.outline = '3px solid #0bff02';
    });

    price.append(inputs);

        function messageApp () {
            if (inputs.value < 1 || typeof inputs.value === 'null' || isNaN(+inputs.value)){
                inputs.style.outline = '3px solid #ff0214';
                inputs.style.color = '#ff0214';
                error.style.color = '#ff0214';
                inputs.placeholder = '';
                inputs.before(error);
                message.remove();
                btn.remove();
            } else {
                inputs.before(message, btn);
                inputs.style.outline = '3px solid #0bff02';
                inputs.style.color = '#0bff02';
                message.style.color = '#0bff02';
                message.textContent = `Текущая цена: ${inputs.value}`;
                inputs.placeholder = '';
                inputs.style.outline = '';
                error.remove();
            };
        
        };

        inputs.addEventListener('focusout', messageApp);

        btn.addEventListener('click', () => {
                message.remove();
                btn.remove();
                inputs.value = '';
            });
    
};
validatePrice();