'use strict';


function setTheme(SunMoon) {
    localStorage.setItem('theme', SunMoon);
    document.documentElement.className = SunMoon;
};

function toggleTheme() {
    if (localStorage.getItem('theme') === 'theme-dark') {
        setTheme('theme-light');
    } else {
        setTheme('theme-dark');
    }
};


function checkedTheme () {
    if (localStorage.getItem('theme') === 'theme-dark') {
        setTheme('theme-dark');
        document.getElementById('slider').checked = false;
    } else {
        setTheme('theme-light');
      document.getElementById('slider').checked = true;
    }
}

checkedTheme();
