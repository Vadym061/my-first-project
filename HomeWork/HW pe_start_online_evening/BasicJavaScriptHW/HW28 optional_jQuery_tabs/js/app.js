'use strict'
;(function($) {
    $(function() {
        $("ul.tabs_item_caption").on("click", ".tabs_item:not(.active)", function() {
            $(this)
                .addClass("active")
                .siblings()
                .removeClass("active")
                .closest("div.tabs")
                .find("div.tabs__content")
                .removeClass("active")
                .eq($(this).index())
                .addClass("active");
        });
    });
})($);