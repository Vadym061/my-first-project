'use strict';


$(document).ready(function(){
	
	$('ul.tabs .tabs-title').click(function(){
		const tab_id = $(this).attr('data-tab');

		$('ul.tabs-content .tab-item').removeClass('active');
		$('.tab-item').removeClass('active');

		$(this).addClass('active');
		$("#"+tab_id).addClass('active');
	})

})