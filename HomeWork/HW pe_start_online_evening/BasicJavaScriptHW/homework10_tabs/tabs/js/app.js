'use strict';

// Технічні вимоги:

// У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. 
// При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
// Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
// Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. 
// При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.




    
    function tabsNavigation (){
        const tabsElement = document.querySelectorAll('.tabs-title');
        const contentItem = document.querySelectorAll('.content-item');

        tabsElement.forEach(tab => {
            tab.addEventListener('click',()=>{
                const target = document.querySelector(tab.dataset.tabTarget)
                    contentItem.forEach(contentItem => {
                        contentItem.classList.remove('active')
                    });
                        tabsElement.forEach(tab => {
                            tab.classList.remove('active');

                        });
                tab.classList.add('active');
                target.classList.add('active');
            });
        })
    }
    tabsNavigation()