"use strict";
// Технічні вимоги:

// Отримати за допомогою модального вікна браузера число, яке введе користувач.
// За допомогою функції порахувати факторіал числа, яке ввів користувач і вивести його на екран.
// Використовувати синтаксис ES6 для роботи зі змінними та функціями.


// Необов'язкове завдання підвищеної складності

// Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів число, або при введенні вказав не число, - запитати число заново 
// (при цьому значенням для нього повинна бути введена раніше інформація).

let number = +prompt('Enter any number','');
    while (number<0 || isNaN(number) || number === 0) {
        number = +prompt('Enter any number AGAIN', '');
    }

function factorialOfNumber(userNumber) {
    let count =1;
        while (userNumber){
            count *= userNumber--;
        }
        return count;
}

alert(`Factorial my number ${number} is: ${factorialOfNumber(number)}`);
console.log(`Factorial my number ${number} is: ${factorialOfNumber(number)}`);

