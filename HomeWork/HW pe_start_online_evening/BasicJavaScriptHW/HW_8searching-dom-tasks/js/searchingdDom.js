'use strict';

//  1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000
//  2. Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
//  3. Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph
//  4. Отримати елементи 
//  5. вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
//  6. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.\



//  1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000
let searchParagraph = document.querySelectorAll('p');
searchParagraph.forEach(elem => {
    elem.style.background = '#ff0000'
});



//  2. Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
let searchId = document.getElementById('optionsList');
console.log(searchId);
let notes = null;

let parentElement = document.getElementById("optionsList");
console.log(parentElement.parentNode != null, parentElement.parentNode.className);

for (let i = 0; i < searchId.childNodes.length; i++) {
    if (searchId.childNodes[i].className == "options-list-item") {
      notes = searchId.childNodes[i];
      break;
    }        
}
console.log(notes);
//  3. Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph

let newParagraph = document.createElement('p');
newParagraph.className = "testParagraph";
newParagraph.innerHTML = "This is a paragraph";
newParagraph.style.color = 'red';
newParagraph.style.textAlign = 'center';

document.body.append(newParagraph)


// 4. Отримати елементи 

let element = document.querySelectorAll('*')
console.log(element)



//  5. вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
let headerClass = document.querySelectorAll('.main-header');
console.log(headerClass)

for (let clasess of headerClass) {
    if(clasess.classList.contains('main-header')){
        clasess.classList.replace('main-header', 'nav-item');
    }
}

// 6. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

function removeElements (){
    var sectionSearch = document.querySelector(".products-title");
    sectionSearch.parentNode.removeChild(sectionSearch);
}
removeElements()

