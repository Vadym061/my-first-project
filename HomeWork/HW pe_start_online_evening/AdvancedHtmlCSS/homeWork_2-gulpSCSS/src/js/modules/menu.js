
export function menuActive (){
    let burgerMenu = document.querySelector('.header__menu-burger');
    let menuList = document.querySelector('.header__menu');

    burgerMenu.addEventListener('click',()=>{
      burgerMenu.classList.toggle("close");
        menuList.classList.toggle("overlay");
      });
};