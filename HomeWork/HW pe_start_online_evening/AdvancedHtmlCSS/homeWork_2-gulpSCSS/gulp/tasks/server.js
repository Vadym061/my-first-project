export const server = (done)=>{
    app.plugins.browserSync.init({
        server:{
            baseDir: `${app.path.build.html}`
        },
        browser: "firefox",
        open: true,
        notify: false,
        port: 3000,
    });
}