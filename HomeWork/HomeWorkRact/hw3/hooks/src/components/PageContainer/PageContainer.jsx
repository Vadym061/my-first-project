import React from "react";
import PropTypes from 'prop-types';
import Container from "../Container/Container";



const PageContainer = ({children}) => {
    return (
        <Container>
            {children}
        </Container> 
    )
}

PageContainer.propTypes = {
    children: PropTypes.any
}

export default PageContainer