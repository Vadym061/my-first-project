import React from 'react';
import PropTypes from 'prop-types';
import Card from '../Card/Card';

const ItemsList =({itemsList, openModal, addToFavorite, textBtn, widthBtn}) => {
    const cardsRender = itemsList.map(({title, price, image, article, color, isInFavorite}, index) => {
        return (
            (<Card data={article} key={(article + '' + index)} isInFavorite={isInFavorite} title={title} price={price} image={image} 
            article={article} color={color} openModal={openModal} addToFavorite={addToFavorite} textBtn={textBtn} widthBtn={widthBtn}/>)
        )
    })
    return (
        <>
            {cardsRender}
        </>
    )
}

ItemsList.propTypes = {
    itemsList: PropTypes.array.isRequired,
    openModal: PropTypes.func,
    addToFavorite: PropTypes.func,
    widthBtn: PropTypes.string
}

export default ItemsList