import PropTypes from 'prop-types';
import PageContainer from "../../components/PageContainer/PageContainer";
import ItemsList from "../../components/ItemsList/ItemsList";
import BackButton from '../../components/BackButton/BackButton';
import "./style.scss";


const Basket = (props) => {
    const {itemsList, openModal, addToFavorite, textBtn, arrow} = props;
    const totalPrice = itemsList.reduce((b, item) => b + +item.price, 0)
    return (
        <PageContainer>
            <div style={{ height: '85vh'}}>
            <div>
                <h2 className='cart__title'><BackButton goBack={arrow} />Cart</h2>
                <p className='price'>Your product {itemsList.length} item(s) with total value {totalPrice}₴</p>
            </div>
            <div className='cart__section'>
                <ItemsList itemsList={itemsList} openModal={openModal} addToFavorite={addToFavorite} textBtn={textBtn}/>
            </div>
            </div>
        </PageContainer>
    )
}

Basket.propTypes = {
    itemsList: PropTypes.array.isRequired,
    openModal: PropTypes.func,
    addToFavorite: PropTypes.func,
    textBtn: PropTypes.string.isRequired,
    arrow: PropTypes.func
}

export default Basket;