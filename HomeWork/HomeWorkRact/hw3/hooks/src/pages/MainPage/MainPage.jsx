import React from "react";
import PropTypes from 'prop-types';
import ItemsList from "../../components/ItemsList/ItemsList";

const MainPage = (props) => {

    const {itemsList, openModal, addToFavorite, textBtn} = props;

    return (
        <ItemsList itemsList={itemsList} openModal={openModal} addToFavorite={addToFavorite} textBtn={textBtn}/>
    )
}

MainPage.propTypes = {
    itemsList: PropTypes.array.isRequired,
    openModal: PropTypes.func,
    addToFavorite: PropTypes.func,
    textBtn: PropTypes.string.isRequired,
}

export default MainPage;