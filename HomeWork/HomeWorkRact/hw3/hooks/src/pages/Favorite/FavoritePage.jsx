import PropTypes from 'prop-types';
import PageContainer from '../../components/PageContainer/PageContainer';
import ItemsList from '../../components/ItemsList/ItemsList';
import BackButton from '../../components/BackButton/BackButton';

const FavoritePage = (props) => {
    const {itemsList, openModal, addToFavorite, textBtn, arrow} = props;
    return (
        <PageContainer>
            <div style={{display: 'flex', gap: '15px', height: '85vh'}}>
                <h2><BackButton arrow={arrow} />Favorite</h2>
                <ItemsList itemsList={itemsList} openModal={openModal} addToFavorite={addToFavorite} textBtn={textBtn}/>
            </div>
        </PageContainer>
    )
}

FavoritePage.propTypes = {
    itemsList: PropTypes.array.isRequired,
    openModal: PropTypes.func,
    addToFavorite: PropTypes.func,
    textBtn: PropTypes.string.isRequired,
    arrow: PropTypes.func
}

export default FavoritePage;