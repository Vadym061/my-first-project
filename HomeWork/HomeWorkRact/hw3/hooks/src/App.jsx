import React, {useEffect, useState} from 'react';
import {Route, Routes, useNavigate} from 'react-router-dom';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import MainPage from './pages/MainPage/MainPage';
import Basket from './pages/Cart/Basket';
import FavoritePage from './pages/Favorite/FavoritePage';
import "./app.scss";
import Container from './components/Container/Container';

const App = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [goodsCollection, setGoodsCollection] = useState([]);
  const [cart, setCart] = useState([]);
  const [tryToCart, setTryToCart] = useState({});
  const [tryToRemoveCart, setTryToRemoveCart] = useState({});
  const [favorite, setFavorite] = useState([]);
  const [modalText, setModalText] = useState('');
  const [modalTitle, setModalTitle] = useState('');
  const [needRemoveItem, setNeddRemoveItem] = useState(false);
  

  useEffect(() => {
    sendRequest();
    getCart();
    getFavorite();
  }, []);

  useEffect(() => {
    if (isModalOpen) modalShow();
  }, [isModalOpen]);

  const sendRequest = () => {
    fetch(`http://localhost:3000/db.json`)
    .then((res) => res.json())
    .then((data) => {
      makeFavorite(data);
    })
  }

  const makeFavorite = (data) => {
    let allGoodsCollection = data;
    let favCollection =  JSON.parse(localStorage.getItem('favorite'));
    if(favCollection) {
      allGoodsCollection.forEach(item => {
        favCollection.forEach(fav => {
          if(item.article === fav.article) {
            item.isInFavorite = true;
          }
        })
      })
    }
    setGoodsCollection(allGoodsCollection);
  }

  const openModal = () => {
    setIsModalOpen(true);
  }
  const modalShow = () => document.querySelector('dialog').showModal();
  const closeModal = () => {
    setIsModalOpen(false);
  }
  
  const confirmAddItem = () => {
    let newCart = [...cart];
    let item = {...tryToCart};
    newCart.push(item);
    setCart(newCart);
    localStorage.setItem('cart', JSON.stringify(newCart));
    closeModal();
  }

  const confirmRemoveItem = () => {
    let newCart = [...cart];
    let item = {...tryToRemoveCart};
    let index = newCart.findIndex((card) => card.article === item.article);
    newCart.splice(index, 1)
    setCart(newCart);
    localStorage.setItem('cart', JSON.stringify(newCart));
    closeModal();
  }

  const addToCart = (event) => {
    let article = +event.target.closest('.card').dataset.article;
    let item = goodsCollection.filter(item => item.article === article)[0];
    setTryToCart(item);
    setModalText(item.name);
    setModalTitle('Do you really want to buy this item?');
    setNeddRemoveItem(false);
    openModal();
  }

  const removeFromCart = (event) => {
    let article = +event.target.closest('.card').dataset.article;
    let item = cart.filter(item => item.article === article)[0];
    setTryToRemoveCart(item);
    setModalText(item.name);
    setModalTitle('Do you want to remove from cart?');
    setNeddRemoveItem(true);
    openModal();
  }

  const addToFavorite = (event) => {
    let article = +event.target.closest('.card').dataset.article;
    let newGoodsCollection = [...goodsCollection];
    let newCart = [...cart];
    
    newGoodsCollection.forEach(item => {
      if(item.article === article) {
        item.isInFavorite = !item.isInFavorite;
      }
    });

    newCart.forEach(item => {
      if(item.article === article) {
        item.isInFavorite = !item.isInFavorite;
      }
    });

    let newFavorite = newGoodsCollection.filter(item => item.isInFavorite);
    setFavorite(newFavorite);
    localStorage.setItem('favorite', JSON.stringify(newFavorite));

    setGoodsCollection(newGoodsCollection);

    setCart(newCart);
    localStorage.setItem('cart', JSON.stringify(newCart));
  }
  
  const getCart = () => {
    let cart = JSON.parse(localStorage.getItem('cart'));
    if(cart && cart.length > 0) {
      setCart(cart);
    }
  }

  const getFavorite = () => {
    let favorite = JSON.parse(localStorage.getItem('favorite'));
    if(favorite) {
      setFavorite(favorite);
    }
  }

  const navigate = useNavigate();
  const arrow = () => navigate(-1);

  return (
    <>
      <Header cartAmount={cart.length} favoriteAmount={favorite.length}/>
      <Container>
      <Routes>
        <Route index element={<MainPage itemsList={goodsCollection} openModal={addToCart} addToFavorite={addToFavorite} textBtn='Add to cart' />} />
        <Route path='/cart' element={<Basket itemsList={cart} openModal={removeFromCart} addToFavorite={addToFavorite} textBtn='Remove from cart' widthBtn='135px' arrow={arrow}/>} />
        <Route path='/favorite' element={<FavoritePage itemsList={favorite} openModal={addToCart} addToFavorite={addToFavorite} textBtn='Add to cart' arrow={arrow}/>} />
      </Routes>
      
      {isModalOpen && (<Modal closeButton={true} actions={
        <>
          <Button backgroundColor='rgba(0, 0, 0, 0.2)' onClick={needRemoveItem ? confirmRemoveItem : confirmAddItem}>Yes</Button>
          <Button backgroundColor='rgba(0, 0, 0, 0.2)' onClick={closeModal}>Cancel</Button>
        </>
      } closeClick={closeModal} backgroundColor='#aeb5b8' header={modalTitle} text={modalText}>
        
      </Modal>)}
      </Container>
      <Footer/>
    </>
  )
}



export default App;
