import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Button/Button';
import "./cards.scss";
import {BsStarFill} from "react-icons/bs"
import { useDispatch, useSelector } from 'react-redux';
import { selectCart, selectGoodsCollection } from '../../store/selectors';
import { actionCart, actionFavorite, actionGoodsCollection, actionTryToCart, actionTryToRemoveCart, actionModalText, actionModalTitle, actionNeedRemoveItem, actionIsModalOpen } from '../../store/actions'


const Card = ({title, price, image, article, color, isRemove, isInFavorite, textBtn}) => {
    const dispatch = useDispatch();
    const cart = useSelector(selectCart);
    const goodsCollection = useSelector(selectGoodsCollection);
    
    const addToFavorite = (article) => {
        let newGoodsCollection = goodsCollection.map(item => item.article === article ? {...item, isInFavorite: !item.isInFavorite} : item);
        let newCart = cart.map(item => item.article === article ? {...item, isInFavorite: !item.isInFavorite} : item);
        let newFavorite = newGoodsCollection.filter(item => item.isInFavorite);
        dispatch(actionFavorite(newFavorite))
        localStorage.setItem('favorite', JSON.stringify(newFavorite));
        dispatch(actionGoodsCollection(newGoodsCollection));
        dispatch(actionCart(newCart));
        localStorage.setItem('cart', JSON.stringify(newCart));
    }

    const addToCart = (article) => {
        let item = goodsCollection.filter(item => item.article === article)[0];
        dispatch(actionTryToCart(item));
        dispatch(actionModalText(item.title));
        dispatch(actionModalTitle(`Do you want to add this item to your shopping cart?`));
        dispatch(actionNeedRemoveItem(false));
        dispatch(actionIsModalOpen(true));
    }

    const removeFromCart = (article) => {
        let item = cart.filter(item => item.article === article)[0];
        dispatch(actionTryToRemoveCart(item));
        dispatch(actionModalText(item.title));
        dispatch(actionModalTitle('Do you want to remove this product to cart?'));
        dispatch(actionNeedRemoveItem(true));
        dispatch(actionIsModalOpen(true));
      }
      return (
        <div className='card'>
            <div className="card__section">
                <span>{title}</span>
                <Button width='auto' height='auto' onClick={() => addToFavorite(article)}>
                    <div>
                        <BsStarFill fontSize="24" fill={isInFavorite ? 'orange' : 'gray'} stroke={isInFavorite ? 'orange' : 'grey'}/>
                    </div>
                </Button>
            </div>
            <div className='card__main'>
                <span className='card__span' fontSize='12px'>Article: {article}</span>
                <div className='card__img'>
                    <img className='card__image' src={image} alt={title} />
                </div>
                <span className='card__span'>Color: {color}</span>
            </div>
            <footer className='card__footer'>
                <div>{price}₴</div>
                <button className='card__btn' onClick={isRemove ? () => removeFromCart(article) : () => addToCart(article)}>{textBtn}</button>
            </footer>
        </div>
    )
}



Card.propTypes = {
    title: PropTypes.string,
    price: PropTypes.string,
    imgUrl: PropTypes.string,
    article: PropTypes.number,
    color: PropTypes.string,
    isRemove: PropTypes.bool,
    isInFavorite: PropTypes.bool,
    textBtn: PropTypes.string.isRequired,
    widthBtn: PropTypes.string
}

export default Card