import PropTypes from 'prop-types';
import {BsFillArrowLeftCircleFill} from "react-icons/bs";
import "./back.scss";


const BackBtn = ({arrow}) => {
    return (
        <a href="/" className="arrow__back" onClick={arrow}><BsFillArrowLeftCircleFill/></a>
    )
}

BackBtn.propTypes = {
    arrow: PropTypes.func
}

export default BackBtn