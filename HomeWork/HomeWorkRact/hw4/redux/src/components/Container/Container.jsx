import React from 'react';
import PropTypes from 'prop-types';
import "./container.scss";


const Container = ({children}) => {
    return  <main className='wrapper'>
                {children}
            </main>
}

Container.propTypes = {
    children: PropTypes.any
}

export default Container
