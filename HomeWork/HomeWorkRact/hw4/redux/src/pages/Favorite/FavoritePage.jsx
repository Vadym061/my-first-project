import PropTypes from 'prop-types';
import ItemsList from '../../components/ItemsList/ItemsList';
import BackBtn from '../../components/BackButton/BackBtn';
import { selectFavorite } from '../../store/selectors';
import { useSelector } from 'react-redux';

const FavoritePage = ({textBtn, arrow}) => {
    const favorite = useSelector(selectFavorite);
    return (
        <div style={{height: "85vh"}}>
            <h2><BackBtn arrow={arrow} />Your favorite</h2>
            <div style={{display:'flex', gap: "20px"}}>
            <ItemsList itemsList={favorite} textBtn={textBtn}/>
            </div>
        </div>
    )
}

FavoritePage.propTypes = {
    textBtn: PropTypes.string.isRequired,
    arrow: PropTypes.func
}

export default FavoritePage;