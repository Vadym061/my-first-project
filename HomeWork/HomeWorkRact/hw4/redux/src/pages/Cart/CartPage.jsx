import PropTypes from 'prop-types';
import ItemsList from "../../components/ItemsList/ItemsList";
import BackBtn from '../../components/BackButton/BackBtn';
import './style.scss';
import { selectCart } from '../../store/selectors';
import { useSelector } from 'react-redux';


const CartPage = (props) => {
    const cart = useSelector(selectCart);

    const {textBtn, widthBtn, arrow} = props;
    const totalPrice = cart.reduce((acc, item) => acc + +item.price, 0)
    return (
        <div style={{height: "85vh"}}>
            <h2><BackBtn arrow={arrow} />Your cart</h2>
            <div className='section'>
                <p className='price'>You have {cart.length} price {totalPrice}₴</p>
                <div style={{display:'flex', gap: "20px"}}>
                <ItemsList itemsList={cart} isRemove={true} textBtn={textBtn} widthBtn={widthBtn}/>
                </div>
            </div>
        </div>
    )
}

CartPage.propTypes = {
    textBtn: PropTypes.string.isRequired,
    widthBtn: PropTypes.string,
    arrow: PropTypes.func
}

export default CartPage;