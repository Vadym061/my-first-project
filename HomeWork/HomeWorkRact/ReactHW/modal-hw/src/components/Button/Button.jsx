import React, { Component } from 'react'
import "./btn.scss";

export default class Button extends Component {
    render() {
        const {text, onClick, backgroundColor} = this.props 
        return (
            <button style={{backgroundColor:backgroundColor}} className="btn"
            onClick = {onClick}>{text}</button>
        )
    }
}
