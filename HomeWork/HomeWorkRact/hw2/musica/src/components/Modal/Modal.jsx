import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styleComponent from 'styled-components';

const ModalWindow = styleComponent.dialog`
    max-width: 600px;
    height: max-content;
    background: ${props => props.backgroundColor};
    border: 1px solid #888;
    border-radius: 8px;
    padding: 0; 
    position: relative;
    top: 50%;
    left: 50%;
    margin-right: -50%;
    transform: translate(-50%, -50%) }

    ::backdrop {
        background: rgba(0, 0, 0, 0.5);
    }
`

const ModalHeader = styleComponent.div`
    display: flex;
    justify-content:space-between;
    background: green;
    padding: 20px;
`
const ModalTitle = styleComponent.h2`
    color: #fff;
    font-weight: 700;
    margin: 0;
`

const ModalText = styleComponent.div`
    color: #fff;
    font-weight: 400;
    padding: 25px 20px;
    display: flex;
    background: #8b8b8b;
    justify-content: center;
    align-items: center;
`

const ModalFooter = styleComponent.div`
    display: flex;
    justify-content:center;
    padding: 10px;
    background: #8b8b8b;
    gap: 10px;
`

const ModalCross = styleComponent.div`
   border: none;
   background: orange;
   cursor:pointer;
   border-radius: 5px;
   padding: 10px;
`



class Modal extends Component {
    constructor(props) {
        super(props);
        this.closeClick = props.closeClick;
    }
    preventAutoClose = (e) => e.stopPropagation();

    escFunction = (e) => {
        if(e.keyCode === 27) {
            this.closeClick();
            console.log('esc is pressed');
        }
      }

      componentDidMount() {
        document.addEventListener("keydown", this.escFunction);
      }

      componentWillUnmount() {
        document.removeEventListener("keydown", this.escFunction);
      }

    
    render() { 
        const {header, text, actions, closeButton,backgroundColor, closeClick} = this.props;
        return (
            <ModalWindow backgroundColor={backgroundColor} onClick={closeClick}>
                <div onClick={this.preventAutoClose}>
                    <ModalHeader>
                        <ModalTitle>{header}</ModalTitle>
                        {closeButton && <ModalCross onClick={closeClick}>X</ModalCross>}
                    </ModalHeader>
                    <ModalText>{text}</ModalText>
                    <ModalFooter>
                        {actions}
                    </ModalFooter>

                </div>
            </ModalWindow>
        )
    }
}

Modal.propTypes = {
    header: PropTypes.string,
    text: PropTypes.string,
    actions: PropTypes.element,
    closeButton: PropTypes.bool,
    backgroundColor: PropTypes.string,
    closeClick: PropTypes.func
}

Modal.defaultProps = {
    closeButton: true,
    backgroundColor: 'green'
}

export default Modal;