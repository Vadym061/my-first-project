import React, {Component} from 'react';
import PropTypes from 'prop-types';
import "./button.scss";

class Button extends Component {
        
    render() {
        const {children, onClick} = this.props;

        return <button className='btn' type="button" onClick={onClick}>{children}</button>
    }
}

Button.propTypes = {
    children: PropTypes.any,
    onClick: PropTypes.func,
}


export default Button;