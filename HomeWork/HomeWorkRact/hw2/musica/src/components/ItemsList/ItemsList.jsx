import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Card from '../Cards/Cards';

export default class ItemsList extends Component {

    render() {
        const {itemsList, openModal, addToFavorite} = this.props;

        const cardsRender = itemsList.map(({title, price, image, article, color, isInFavorite}) => {
            return (
                <Card data={article} key={article} isInFavorite={isInFavorite} title={title} price={price} image={image} article={article} color={color} openModal={openModal} addToFavorite={addToFavorite}/>
            )
        })
        return (
            <>
                    {cardsRender}
            </>
        )
    }
}

ItemsList.propTypes = {
    itemsList: PropTypes.array.isRequired,
    openModal: PropTypes.func,
    addToFavorite: PropTypes.func
}