import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '../Button/Button';
import "./cards.scss";
import {BsStarFill} from "react-icons/bs"



export default class Card extends Component {


    render() {
        const {title, price, image, article, color, openModal, data, isInFavorite, addToFavorite} = this.props;
        return (
            <div data-article={data} className="card">
                <div className="card__section">
                    <span>{title}</span>
                    <Button width='auto' height='auto' onClick={addToFavorite}>
                        <div>
                            <BsStarFill fontSize="24" fill={isInFavorite ? 'orange' : 'gray'}/>
                        </div>
                    </Button>
                </div>
                <div className='card__main'>
                    <span className='card__span'>Article: {article}</span>
                    <div className='card__img'>
                        <img className='card__image' src={image} alt={title} />
                    </div>
                    <span className='card__span'>Color: {color}</span>
                </div>
                <footer className='card__footer'>
                    <div>{price}UAH</div>
                    <button className='card__btn' onClick={openModal}>Add to cart</button>
                </footer>
            </div>
        )
    }
}

Card.propTypes = {
    title: PropTypes.string,
    price: PropTypes.string,
    image: PropTypes.string,
    article: PropTypes.number,
    color: PropTypes.string,
    openModal: PropTypes.func,
    addToFavorite: PropTypes.func,
    isInFavorite: PropTypes.bool,
}