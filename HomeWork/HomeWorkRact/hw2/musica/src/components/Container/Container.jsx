import React, { Component } from 'react';
import PropTypes from 'prop-types';
import "./container.scss";

export default class Container extends Component {
    
    render() {
        const {children} = this.props
        return  <main className='wrapper'>
                    {children}
                </main>
    }
}

Container.propTypes = {
    children: PropTypes.element
}