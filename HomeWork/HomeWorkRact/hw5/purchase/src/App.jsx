import React, {useEffect} from 'react';
import {Route, Routes, useNavigate} from 'react-router-dom';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import MainPage from './pages/MainPage/MainPage';
import CartPage from './pages/Cart/CartPage';
import "./app.scss";
import FavoritePage from './pages/Favorite/FavoritePage';
import { useDispatch, useSelector } from 'react-redux';
import { selectCart, selectIsModalOpen, selectModalText, selectModalTitle, selectNeedRemoveItem, selectTryToCart, selectTryToRemoveCart } from './store/selectors';
import { actionCart, actionFavorite, actionFetchGoodsCollection, actionIsModalOpen } from './store/actions';

const App = () => {
  const dispatch = useDispatch();

  const isModalOpen = useSelector(selectIsModalOpen);
  const modalText = useSelector(selectModalText);
  const modalTitle = useSelector(selectModalTitle);
  const cart = useSelector(selectCart);
  const needRemoveItem = useSelector(selectNeedRemoveItem);
  const tryToCart = useSelector(selectTryToCart);
  const tryToRemoveCart = useSelector(selectTryToRemoveCart);

  useEffect(() => {
    dispatch(actionFetchGoodsCollection());
    getCart();
    getFavorite();
  }, [])

  useEffect(() => {
    if (isModalOpen) modalShow();
  }, [isModalOpen]);

  const modalShow = () => document.querySelector('dialog').showModal();

  const closeModal = () => {
    dispatch(actionIsModalOpen(false));
  }
  
  const confirmAddItem = () => {
    let newCart = [...cart];
    let item = {...tryToCart};
    newCart.push(item);
    dispatch(actionCart(newCart));
    localStorage.setItem('cart', JSON.stringify(newCart));
    closeModal();
  }

  const confirmRemoveItem = () => {
    let newCart = [...cart];
    let item = {...tryToRemoveCart};
    let index = newCart.findIndex((card) => card.article === item.article);
    newCart.splice(index, 1);
    dispatch(actionCart(newCart));
    localStorage.setItem('cart', JSON.stringify(newCart));
    closeModal();
  }

  const getCart = () => {
    let cart = JSON.parse(localStorage.getItem('cart'));
    if(cart && cart.length > 0) {
      dispatch(actionCart(cart))
    }
  }

  const getFavorite = () => {
    let favorite = JSON.parse(localStorage.getItem('favorite'));
    if(favorite) {
      dispatch(actionFavorite(favorite))
    }
  }

  const navigate = useNavigate();
  const goBack = () => navigate(-1);

  return (
    <>
      <Header/>
      <Routes>
        <Route index element={<MainPage textBtn='Add to cart' />} />
        <Route path='/cart' element={<CartPage textBtn='Remove from cart' widthBtn='135px' goBack={goBack}/>} />
        <Route path='/favorite' element={<FavoritePage textBtn='Add to cart' goBack={goBack}/>} />
      </Routes>
      
      {isModalOpen && (<Modal closeButton={true} actions={
        <>
          <Button backgroundColor='rgba(0, 0, 0, 0.2)' onClick={needRemoveItem ? confirmRemoveItem : confirmAddItem}>Yes</Button>
          <Button backgroundColor='rgba(0, 0, 0, 0.2)' onClick={closeModal}>Cancel</Button>
        </>
      } closeClick={closeModal} backgroundColor='#aeb5b8' header={modalTitle} text={modalText}>
        
      </Modal>)}
      
      <Footer/>
    </>
  )
}


export default App;
