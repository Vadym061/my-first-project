import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import ItemsList from "../../components/ItemsList/ItemsList";
import BackBtn from '../../components/BackButton/BackBtn';
import './style.scss';
import { selectCart } from '../../store/selectors';
import { useSelector } from 'react-redux';
import { actionCart } from '../../store/actions';
import FormCard from "../../components/Form/FormCard/Form";

const CartPage = (props) => {
    const dispatch = useDispatch();
    const cart = useSelector(selectCart);

    const purchaseSubmit = (value) => {
        value.phone = value.phone.match(/\d+/g).join('');
        if(cart.length !== 0) {
            let order = {
                user: value,
                cart: cart
            }
            console.log(order);
            localStorage.removeItem('cart');
            dispatch(actionCart([]));
        } else console.log('Your cart is empty');
    }
    const {textBtn, widthBtn, arrow} = props;
    const totalPrice = cart.reduce((acc, item) => acc + +item.price, 0)
    return (
        <div>
            <h2><BackBtn arrow={arrow} />Your cart</h2>
            <div className='section'>
            {cart.length > 0 && <FormCard totalPrice={totalPrice} amount={cart.length} onSubmit={purchaseSubmit}/>}
            {cart.length === 0 && <h2>Your cart is empty. Please add some goods</h2>}
                <div style={{display:'flex', gap: "20px", flexWrap: "wrap"}}>
                <ItemsList itemsList={cart} isRemove={true} textBtn={textBtn} widthBtn={widthBtn}/>
                </div>
            </div>
        </div>
    )
}

CartPage.propTypes = {
    textBtn: PropTypes.string.isRequired,
    widthBtn: PropTypes.string,
    arrow: PropTypes.func
}

export default CartPage;