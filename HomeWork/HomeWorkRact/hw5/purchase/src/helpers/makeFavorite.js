const makeFavorite = (data) => {

    let allGoodsCollection = data;
    let favCollection =  JSON.parse(localStorage.getItem('favorite'));
    if(favCollection) {
      allGoodsCollection.forEach(item => {
        favCollection.forEach(fav => {
          if(item.article === fav.article) {
            item.isInFavorite = true;
          }
        })
      })
    }
    return allGoodsCollection
  }

export default makeFavorite