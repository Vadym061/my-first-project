import React from 'react';
import PropTypes from 'prop-types';
import "./button.scss";


const Button = ({children, onClick, type}) => {
    return <button className='btn' type={type ? type : "button"} onClick={onClick}>{children}</button>
        
}

Button.propTypes = {
    children: PropTypes.any,
    onClick: PropTypes.func,
}

export default Button;