import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import styleComponent from 'styled-components';

const ModalWindow = styleComponent.dialog`
    max-width: 600px;
    height: max-content;
    background: ${props => props.backgroundColor};
    border: 1px solid #888;
    border-radius: 8px;
    padding: 0; 
    position: relative;
    top: 50%;
    left: 50%;
    margin-right: -50%;
    transform: translate(-50%, -50%) }

    ::backdrop {
        background: rgba(0, 0, 0, 0.5);
    }
`


const ModalHeader = styleComponent.div`
    display: flex;
    justify-content:space-between;
    background: green;
    padding: 20px;
`
const ModalTitle = styleComponent.h2`
    color: #fff;
    font-weight: 700;
    margin: 0;
`

const ModalText = styleComponent.div`
    color: #fff;
    font-weight: 400;
    padding: 25px 20px;
    display: flex;
    justify-content: center;
    align-items: center;
`

const ModalFooter = styleComponent.div`
    display: flex;
    justify-content:center;
    padding: 10px;
    gap: 10px;
`

const ModalCross = styleComponent.div`
    width: 8px;
    height: 8px;
    position: relative;
    top: 15px;
    right: 10px;
    cursor: pointer;
    ::before, ::after {
        content: '';
        width: 20px;
        height: 2px;
        background: #fff;
        position: absolute;
    }
    ::before {transform: rotate(45deg);}
    ::after {transform: rotate(-45deg);}
`

const Modal = ({header, text, actions, closeButton, backgroundColor, closeClick}) => {
    const preventAutoClose = (e) => e.stopPropagation();

    const escFunction = (e) => {
        if(e.keyCode === 27) {
            closeClick();
        }
      }

      useEffect(() => {
        document.addEventListener("keydown", escFunction);

        return function clean() {
            document.removeEventListener("keydown", escFunction);
        }
      })
      
      return (
          <ModalWindow backgroundColor={backgroundColor} onClick={closeClick}>
              <div onClick={preventAutoClose}>
                  <ModalHeader>
                      <ModalTitle>{header}</ModalTitle>
                      {closeButton && <ModalCross onClick={closeClick}/>}
                  </ModalHeader>
                  <ModalText>{text}</ModalText>
                  <ModalFooter>
                      {actions}
                  </ModalFooter>
              </div>
          </ModalWindow>
      )
    
}


Modal.propTypes = {
    header: PropTypes.string,
    text: PropTypes.string,
    actions: PropTypes.element,
    closeButton: PropTypes.bool,
    backgroundColor: PropTypes.string,
    closeClick: PropTypes.func
}

Modal.defaultProps = {
    closeButton: true,
    backgroundColor: 'grey'
}

export default Modal;