import React from "react";
import PropTypes from 'prop-types';
import Container from "../Container/Container";


const PageWrapper = ({children}) => {
    return (
        <div style={{display:"flex"}}>
        <Container>
            {children}
        </Container>
        </div>
    )
}

PageWrapper.propTypes = {
    children: PropTypes.any
}

export default PageWrapper