import { Formik, Form } from "formik";
import PropTypes from 'prop-types';
import Button from "../../Button/Button";
import Input from "../Input/Input";
import { validationSchema } from "./validte.js";
import "./form.scss";


const CartForm = ({amount, totalPrice, onSubmit}) => {
    return (
        <Formik
            initialValues = {
                {
                    name: "Ivan",
                    surname: "Pupkin",
                    adress: "15 Ovanesa Tumanyana Kyiv, 02002",
                    age: '30',
                    phone: ""
                }
            }
            onSubmit={(values) => onSubmit(values)}
            validationSchema={validationSchema}
        >
            {({errors, touched}) => (
            <div className="form">
                <Form style={{width: '100%'}}>
                    <header className="form__title">Please fill out the purchase form</header>
                        <div className="form__wrapper">
                            <Input name="name" label='Your name' placeholder="Your name"
                                    errors={errors} error={errors.name && touched.name}/>

                            <Input name="surname" label='Your lastname' placeholder="Your lastname"
                                    error={errors.surname && touched.surname}/>

                            <Input name="age" label='Age' placeholder="Age" type='number'
                                    error={errors.age && touched.age}/>

                            <Input name="phone" label='Phone' placeholder="Phone"
                                    error={errors.phone && touched.phone} phone={true}/>
                        </div>
                        <div className="form__wrapper">
                            <Input name="adress" label='Delivery address' placeholder="Delivery address"
                                    error={errors.adress && touched.adress} width='50%'/>
                        </div>
                        <p className="form__card-price"> Selected products {amount} total cost {totalPrice}₴</p>
                    <div className="form__btn">
                        <Button className='form__button' type="submit">Checkout</Button>
                    </div>

                </Form>
            </div>
            )}
        </Formik>
    )
}

CartForm.propTypes = {
    amount: PropTypes.number,
    totalPrice: PropTypes.number,
    onSubmit: PropTypes.func
}

export default CartForm