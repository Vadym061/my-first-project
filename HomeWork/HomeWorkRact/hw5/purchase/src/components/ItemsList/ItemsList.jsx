import React from 'react';
import PropTypes from 'prop-types';
import Card from '../Card/Card';


const ItemsList =({itemsList, isRemove, textBtn, widthBtn}) => {
    const cardsRender = itemsList.map(({title, price, image, article, color, isInFavorite}, index) => {
        return (
            (<Card key={(article + '' + index)} isInFavorite={isInFavorite} title={title} price={price} image={image} 
            article={article} color={color} isRemove={isRemove} textBtn={textBtn} widthBtn={widthBtn}/>)
        )
    })
    return (
        <>
            {cardsRender}
        </>
    )
}

ItemsList.propTypes = {
    itemsList: PropTypes.array.isRequired,
    isRemove: PropTypes.bool,
    addToFavorite: PropTypes.func,
    widthBtn: PropTypes.string
}

export default ItemsList