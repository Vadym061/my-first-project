import { Component } from 'react'
import "./footer.scss";

export default class Footer extends Component {
    render() {
        return (
            <footer className="footer">
                <p className="copy">all rights reserved &#169; 2022</p>
            </footer>
        )
    }
}
