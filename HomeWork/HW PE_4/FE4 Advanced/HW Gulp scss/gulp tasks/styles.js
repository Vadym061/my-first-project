const { src, dest } = require("gulp");
const { bs } = require("./serv.js");
const sass = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const sourcemaps = require("gulp-sourcemaps");
const autoprefixer = require("gulp-autoprefixer");


function styles() {
   return src("./src/styles/*.scss")
      .pipe(sourcemaps.init())
      .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
      .pipe(autoprefixer( {
         browsers:['last 5 versions']
      }))
      .pipe(concat('style.min.css'))
      .pipe(sourcemaps.write())
      .pipe(dest("./dist/css"))
      .pipe(bs.reload({ stream: true }));
}

exports.styles = styles;
