const { src, dest } = require("gulp");
const { bs } = require("./serv.js");

function images() {
   return src("./src/images/**/*.{jpg,jpeg,png,gif,tiff,svg}")
      .pipe(dest("./dist/img"))
      .on("end", bs.reload);
}

exports.images = images;
