const { src, dest } = require("gulp");
const { bs } = require("./serv.js");


function scripts() {
   return src("./src/js/*.js")
      .pipe(dest("./dist/js"))
      .pipe(bs.reload({ stream: true }));
}

exports.scripts = scripts;
