"use strict";

class Employee{
    constructor(name, age, salary){
        this._name=name;
        this._age=age;
        this._salary=salary;

    }
    set name(value){
        this._name=value
    }
    get name(){
        return this._name
    };
    set age(value){
        this._age=value;
    }
    get age(){
        return this._age
    };
    set salary(value){
        this._salary=value
    }
    get salary(){
        return this._salary
   }
};

class Programmer extends Employee {
    constructor(name, age, salary,lang){
        super(name, age, salary)
        this.lang=lang
    };
   get salary(){
        return super.salary*3
   }
};

const userVadym = new Programmer("Vadym", 30, 20000, ["HTML","CSS","JS","ReactJS"]);
const userElena = new Programmer("Elena", 35, 15000, ["JS", "Angular", "jQuery"]);
const userSophia = new Programmer("Sophia", 25, 10000, ["Java", "Kotlin"]);
console.log(userVadym);
console.log(userElena);
console.log(userSophia);