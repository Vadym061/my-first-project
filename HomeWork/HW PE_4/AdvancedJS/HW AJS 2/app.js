// Когда уместно использовать в коде конструкцию try...catch?
// тогда когда, подрузумивается ошибки или не стандартное поведение программы, а так же когда
// принятие ришения об обработке результата необходимо предложить на более высокий уровень. 
// Использование исключений предпологается что весь код выполняется со статусом "true",
// но если ошибка произошла то для ее оброботки всегда найдется блок catch.


const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];

  const root = document.getElementById("root");
  const list = document.createElement("ul");
  root.appendChild(list);
  
  
  let number = 0;
  
  function itemList(arr) {
    for (const i of arr) {
      if (Object.hasOwn(i, "author") && Object.hasOwn(i, "name") && Object.hasOwn(i, "price")) {
        const title = document.createElement('h2');
        number++;
        title.textContent = `Book: ${number}`;
        title.style.color = '#0000ff';
  
        list.appendChild(title);
      } else {
      }
  
      for (const [mr, ls] of Object.entries(i)) {
        const listElement = document.createElement("li");
        try {
          if (Object.hasOwn(i, "author") && Object.hasOwn(i, "name") && Object.hasOwn(i, "price")
          ) {
            listElement.textContent = mr + ": " + ls;
            list.appendChild(listElement);
          } else {
            if (!Object.hasOwn(i, "author")) {
              throw new Error("there is no property in the object: author");
            }
            if (!Object.hasOwn(i, "name")) {
              throw new Error("there is no property in the object: name");
            }
            if (!Object.hasOwn(i, "price")) {
              throw new Error("there is no property in the object: price");
            }
          }
        } catch (e) {
          console.log(e.stack);
        }
      }
    }
  }
  
  itemList(books);