'use strict'

let span = document.createElement('span');
let error = document.createElement('error');
let price = document.getElementById('price');
let input = document.createElement('input');
let buttons = document.createElement('btn');
buttons.innerHTML = `&#9746;`;
input.placeholder = 'PRICE';
error.textContent = 'Please enter correct price';

input.addEventListener('focusin', () => {
    input.style.outline = '3px solid lime';
});
price.append(input);
function spanFailure () {
    if (input.value < 1 || typeof input.value === 'null' || isNaN(+input.value)){
        input.style.outline = '3px solid red';
        input.style.color = 'red';
        input.placeholder = '';
        input.before(error);
        span.remove();
        buttons.remove();
    } else {
        input.before(span, buttons);
        input.style.outline = '3px solid lime';
        input.style.color = 'lime';
        span.textContent = `Текущая цена: ${input.value}`;
        input.placeholder = '';
        input.style.outline = '';
        error.remove();
    }

}
input.addEventListener('focusout', spanFailure);
buttons.addEventListener('click', () => {
    span.remove();
    buttons.remove();
    input.value = '';
})

