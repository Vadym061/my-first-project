'use strict'

const pswrd_1 = document.querySelector('#password1');
const pswrd_2 = document.querySelector('#password2');
const errorText = document.querySelector('.error-text');
const btn = document.querySelector('button');
const error = document.getElementById('error');
const ins = document.querySelectorAll('input');
const iconsPassword = document.querySelectorAll('.icon-password');

function active(){
  if(password1.value.length >= 0){
    btn.removeAttribute('disabled', '');
    btn.classList.add('active');
    password2.removeAttribute('disabled', '');
  }else{
    btn.setAttribute('disabled', '');
    btn.classList.remove('active');
    password2.setAttribute('disabled', '');
  }
};
iconsPassword.forEach(visability);
function visability(icon, index){
    icon.addEventListener('click',ClikIcon);
function ClikIcon(){
    if( icon.className === 'fas fa-eye-slash icon-password'){
        icon.className = 'fas fa-eye icon-password';
        setInputType(ins[index].id,'password');
        
    }else{
        icon.className = 'fas fa-eye-slash icon-password';
        setInputType(ins[index].id,'text');
    }
    }
};
btn.onclick = function(){
  if(password1.value != password2.value){
    errorText.style.display = 'block';
    errorText.classList.remove('matched');
    error.innerHTML = 'Нужно ввести одинаковые значения';
    document.getElementById('error').style.color = 'red';
    return false;
  }else{
    errorText.style.display = 'block';
    errorText.classList.add('matched');
    error.innerHTML = 'Youare welcome';
    document.getElementById('error').style.color = 'lime';
    return false;
  }
};

function setInputType(input,type){
  let x  = document.getElementById(input);
  x.type = type;
};


