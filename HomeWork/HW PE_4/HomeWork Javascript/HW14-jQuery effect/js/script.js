'use strict'

  $('a').on('click', function(event) {
      if (this.hash !== '') {
        event.preventDefault();
        let hash = this.hash    
        $('html, body').animate({
          scrollTop: $(hash).offset().top
        }, 1500, function(){
          window.location.hash = hash;
        });
      } 
  });

  $('li.menu-item a').on('click', function() {
    $('li.menu-item a').removeClass('active_anchor');
    $(this).addClass('active_anchor');
  });


  $(window).scroll(function() {
    let height = $(window).scrollTop();
    if (height > 300) {
        $('#arrowUp').fadeIn();
    } else {
        $('#arrowUp').fadeOut();
    }
});
$(document).ready(function() {
    $('#arrowUp').click(function(event) {
        event.preventDefault();
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    });

});

$(document).ready( function(){
    $('#slide').click(function(){
      $('#news').slideToggle(1000,'linear');
    });
  });