'use strict';

const buttons = [...document.getElementsByClassName("btn")];

document.addEventListener("keydown", (e) => {
    let target = e.key;

    buttons.forEach((el) => {
        if(e.code === `Key${el.textContent}` || e.code === el.textContent){
            el.style.backgroundColor = "blue";
            }else{
                el.style.backgroundColor = "";
            }
    })
})