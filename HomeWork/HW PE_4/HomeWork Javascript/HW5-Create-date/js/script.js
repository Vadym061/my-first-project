"use strict";

function CreateNewUser(){
		this.firstName = prompt("Введите Ваше имя");
		this.lastName = prompt("Введите Вашу фамилию");
		this.birthday = prompt("Введите дату Вашего рождения");
	this.getLogin = function() {
		return `${this.firstName[0].toUpperCase()}.${this.lastName.toLowerCase()}`;
	};

	this.getAge = function() {
		const inThisDay = new Date();
		const userBirthday = Date.parse(`${this.birthday.slice(6)}-${this.birthday.slice(3, 5)}-${this.birthday.slice(0, 2)}`);
		const age = ((inThisDay - userBirthday) / (1000 * 60 * 60 * 24 * 30 * 12)).toFixed(0);
		if (age < inThisDay) {
			return `${age - 1} лет`;
		} else {
			return `${age} лет`;
		}
	};

	this.getPassword = function() {
		return `${this.firstName[0].toUpperCase()}${this.lastName.toLocaleLowerCase()}${this.birthday.slice(-4)}`
	};
}

const newUser = new CreateNewUser();
console.log(`Ваш логин: ${newUser.getLogin()}`);
console.log(`Ваш возрост: ${newUser.getAge()}`);
console.log(`Ваш пароль: ${newUser.getPassword()}`);

